# Machine learning web app using PyCaret and Streamlit
#### This project is a test to train skills and show my work.


References

https://medium.com/@moez_62905/build-and-deploy-machine-learning-web-app-using-pycaret-and-streamlit-28883a569104

Official Website : https://www.pycaret.org

PyCaret YouTube : https://www.youtube.com/channel/UCxA1YTYJ9BEeo50lxyI_B3g

PyCaret repository : https://www.github.com/pycaret/pycaret
