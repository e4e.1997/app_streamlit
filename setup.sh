mkdir -p ~/.streamlit/
echo "\
[general]\n\
email = \"e4e.1997l@gmail.com\"\n\
" > ~/.streamlit/credentials.toml
echo "\
[server]\n\
headless = true\n\
enableCORS=false\n\
port = $PORT\n\
" > ~/.streamlit/config2.toml
